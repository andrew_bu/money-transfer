CREATE TABLE IF NOT EXISTS account (
  id bigint NOT NULL PRIMARY KEY,
  name varchar(255) DEFAULT NULL,
  balance numeric NOT NULL DEFAULT 0,
  created_date timestamp NOT NULL,
  updated_date timestamp NOT NULL,
  CHECK (balance >= 0)
);

CREATE SEQUENCE IF NOT EXISTS account_seq;


CREATE TABLE IF NOT EXISTS `transaction` (
  id bigint NOT NULL PRIMARY KEY,
  from_account_id bigint NOT NULL,
  to_account_id bigint NOT NULL,
  amount numeric NOT NULL DEFAULT 0,
  complete boolean NOT NULL,
  created_date timestamp NOT NULL,
  updated_date timestamp NOT NULL,
  CHECK (amount > 0),
  FOREIGN KEY (from_account_id) REFERENCES account(id),
  FOREIGN KEY (to_account_id) REFERENCES account(id)
);

CREATE SEQUENCE IF NOT EXISTS transaction_seq;
