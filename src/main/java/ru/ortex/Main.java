package ru.ortex;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ortex.api.AccountCreateReq;
import ru.ortex.api.TransactionCreateReq;
import ru.ortex.exception.BadRequestException;
import ru.ortex.service.AccountService;
import ru.ortex.service.TransactionalService;
import spark.Request;
import spark.ResponseTransformer;

import javax.sql.DataSource;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.Properties;

import static spark.Spark.*;

public class Main {

    private static final String CONFIG = "application.properties";
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final ResponseTransformer JSON_RENDER = MAPPER::writeValueAsString;

    public static void main(String[] args) {

        Properties properties = getProperties();
        HikariConfig config = new HikariConfig(properties);
        DataSource dataSource = new HikariDataSource(config);
        ContextManager contextManager = new ContextManager(dataSource);

        AccountService accountService = new AccountService(contextManager);
        TransactionalService transactionalService = new TransactionalService(contextManager);

        post("/accounts", (request, response) -> {
            AccountCreateReq accountCreateReq = parseBody(request, AccountCreateReq.class);
            long accId = accountService.create(accountCreateReq);
            return Collections.singletonMap("id", accId);
        }, JSON_RENDER);

        get("/account/:id", (request, response) -> {
            long id = getParamAsLong(request, ":id");
            return accountService.getById(id);
        }, JSON_RENDER);

        get("/account/full/:id", (request, response) -> {
            long id = getParamAsLong(request, ":id");
            return accountService.getFullById(id);
        }, JSON_RENDER);

        post("/transactions", (request, response) -> {
            TransactionCreateReq transactionCreateReq = parseBody(request, TransactionCreateReq.class);
            long transactionId = transactionalService.transfer(transactionCreateReq);
            return Collections.singletonMap("id", transactionId);
        }, JSON_RENDER);

        get("/transaction/:id", (request, response) -> {
            long id = getParamAsLong(request, ":id");
            return transactionalService.getById(id);
        }, JSON_RENDER);


        before("/*", (request, response) -> LOGGER.trace("Incoming request: " + request.pathInfo()));

        exception(BadRequestException.class, (exception, request, response) -> {
            response.status(400);
            ObjectNode node = MAPPER.createObjectNode();
            node.put("error", exception.getMessage());
            response.body(node.toString());
            LOGGER.trace("Request(" + request.pathInfo() + ") handle bad request exception", exception);
        });

        exception(Exception.class, (exception, request, response) -> {
            response.status(500);
            ObjectNode node = MAPPER.createObjectNode();
            node.put("error", exception.getMessage());
            response.body(node.toString());
            LOGGER.error("Request(" + request.pathInfo() + ") handle exception", exception);
        });
    }

    private static Properties getProperties() {
        URL configUrl = Main.class.getClassLoader().getResource(CONFIG);
        if (configUrl == null) {
            throw new RuntimeException("Not fount config (" + CONFIG + ")");
        }
        Properties properties = new Properties();
        try {
            properties.load(configUrl.openStream());
        } catch (IOException e) {
            throw new RuntimeException("Error on load config (" + CONFIG + ")", e);
        }
        return properties;
    }

    private static long getParamAsLong(Request request, String param) {
        String value = request.params(param);
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            throw new BadRequestException("Invalid request data");
        }
    }

    private static <T> T parseBody(Request request, Class<T> clazz) {
        try {
            return MAPPER.readValue(request.bodyAsBytes(), clazz);
        } catch (JsonParseException | JsonMappingException e) {
            LOGGER.debug("error parse request body: " + e.getMessage());
            throw new BadRequestException("Invalid request data");
        } catch (IOException e) {
            LOGGER.error("error parse request body", e);
            throw new BadRequestException("Invalid request data");
        }
    }

}
