package ru.ortex.exception;

public class Exceptions {

    public static <T> T uncheck(ThrowingSupplier<T> fun) {
        try {
            return fun.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @FunctionalInterface
    public interface ThrowingSupplier<T> {
        T get() throws Exception;
    }

}
