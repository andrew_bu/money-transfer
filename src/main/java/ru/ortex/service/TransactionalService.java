package ru.ortex.service;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SelectConditionStep;
import ru.ortex.ContextManager;
import ru.ortex.api.TransactionCreateReq;
import ru.ortex.exception.BadRequestException;
import ru.ortex.generated.tables.pojos.Account;
import ru.ortex.generated.tables.pojos.Transaction;
import ru.ortex.generated.tables.records.AccountRecord;
import ru.ortex.generated.tables.records.TransactionRecord;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;

import static ru.ortex.generated.Sequences.TRANSACTION_SEQ;
import static ru.ortex.generated.Tables.ACCOUNT;
import static ru.ortex.generated.Tables.TRANSACTION;

public class TransactionalService {

    private final ContextManager contextManager;

    public TransactionalService(ContextManager contextManager) {
        this.contextManager = contextManager;
    }

    public Transaction getById(long id) {
        return contextManager.run(ctx -> getById(ctx, id));
    }

    private Transaction getById(DSLContext ctx, long id) {
        return ctx.select()
                .from(TRANSACTION)
                .where(TRANSACTION.ID.eq(id))
                .fetchAnyInto(Transaction.class);
    }

    public long transfer(TransactionCreateReq req) {
        return transfer(req.fromAccId, req.toAccId, req.amount);
    }

    public long transfer(long fromAccId, long toAccId, BigDecimal amount) {
        if (fromAccId == toAccId) {
            throw new BadRequestException("Source and target accounts should be different!");
        }

        long trId = contextManager.run(ctx -> {

            Account fromAccount = fetchAccount(ctx, fromAccId, true);
            AccountService.check(fromAccount, fromAccId);

            if (fromAccount.getBalance().subtract(amount).compareTo(BigDecimal.ZERO) < 0) {
                String msg = withdrawErrorMsg(fromAccount, amount);
                throw new BadRequestException(msg);
            }

            // Just check existing account
            Account toAccount = fetchAccount(ctx, toAccId, false);
            AccountService.check(toAccount, toAccId);

            Timestamp now = Timestamp.from(Instant.now());
            long transactionId = ctx.nextval(TRANSACTION_SEQ);

            Transaction transaction = new Transaction(transactionId, fromAccId, toAccId, amount, false, now, now);
            ctx.newRecord(TRANSACTION, transaction).insert();

            updateBalance(ctx, fromAccount, fromAccount.getBalance().subtract(amount), now);
            return transactionId;
        });

        return contextManager.run(ctx -> {
            Transaction transaction = getById(trId);
            Timestamp now = Timestamp.from(Instant.now());

            TransactionRecord transactionRecord = ctx.newRecord(TRANSACTION, transaction);
            transactionRecord.setComplete(true);
            transactionRecord.setUpdatedDate(now);
            transactionRecord.update();

            Account account = fetchAccount(ctx, toAccId, true);

            updateBalance(ctx, account, account.getBalance().add(amount), now);
            return trId;
        });
    }

    private Account fetchAccount(DSLContext ctx, long accId, boolean forUpdate) {
        SelectConditionStep<Record> select = ctx.select()
                .from(ACCOUNT)
                .where(ACCOUNT.ID.eq(accId));

        if (forUpdate) {
            return select.forUpdate().fetchAnyInto(Account.class);
        }
        return select.fetchAnyInto(Account.class);
    }

    private void updateBalance(DSLContext context, Account account, BigDecimal newBalance, Timestamp now) {
        AccountRecord accountRecord = context.newRecord(ACCOUNT, account);
        accountRecord.setBalance(newBalance);
        accountRecord.setUpdatedDate(now);
        accountRecord.update();
    }

    private static String withdrawErrorMsg(Account account, BigDecimal amount) {
        return String.format("Account[%s] not enough money (%s) to transfer %s", account.getId(), account.getBalance(), amount);
    }
}
