package ru.ortex.service;

import ru.ortex.ContextManager;
import ru.ortex.api.AccountCreateReq;
import ru.ortex.api.AccountInfo;
import ru.ortex.exception.BadRequestException;
import ru.ortex.generated.tables.pojos.Account;
import ru.ortex.generated.tables.pojos.Transaction;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Map;

import static ru.ortex.generated.Sequences.ACCOUNT_SEQ;
import static ru.ortex.generated.Tables.ACCOUNT;
import static ru.ortex.generated.Tables.TRANSACTION;

public class AccountService {

    private final ContextManager contextManager;

    public AccountService(ContextManager contextManager) {
        this.contextManager = contextManager;
    }

    public long create(String name, BigDecimal balance) {

        return contextManager.run(ctx -> {
            long accountId = ctx.nextval(ACCOUNT_SEQ);

            Timestamp now = Timestamp.from(Instant.now());

            Account account = new Account(accountId, name, balance, now, now);
            ctx.newRecord(ACCOUNT, account).insert();

            return accountId;
        });
    }

    public long create(AccountCreateReq info) {
        return create(info.name, info.balance);
    }

    public List<Account> selectAll() {
        return contextManager.run(ctx -> ctx.select().from(ACCOUNT).fetchInto(Account.class));
    }

    public Account getById(long id) {

        return contextManager.run(ctx -> {
            Account account = ctx.select()
                    .from(ACCOUNT)
                    .where(ACCOUNT.ID.eq(id))
                    .fetchAnyInto(Account.class);
            return check(account, id);
        });
    }

    public AccountInfo getFullById(long id) {

        return contextManager.run(ctx -> {
            Map<Account, List<Transaction>> map = ctx
                    .select(ACCOUNT.fields())
                    .select(TRANSACTION.fields())
                    .from(ACCOUNT)
                    .join(TRANSACTION).on(TRANSACTION.FROM_ACCOUNT_ID.eq(ACCOUNT.ID).or(TRANSACTION.TO_ACCOUNT_ID.eq(ACCOUNT.ID)))
                    .where(ACCOUNT.ID.eq(id))
                    .fetchGroups(
                            r -> r.into(ACCOUNT).into(Account.class),
                            r -> r.into(TRANSACTION).into(Transaction.class)
                    );
            if (map.isEmpty()) {
                throw new BadRequestException("Account[" + id + "] not found");
            }
            Map.Entry<Account, List<Transaction>> entry = map.entrySet().iterator().next();

            return new AccountInfo(entry.getKey(), entry.getValue());
        });
    }


    public static Account check(Account account, long accId) {
        if (account == null) {
            throw new BadRequestException("Account[" + accId + "] not found");
        }
        return account;
    }
}
