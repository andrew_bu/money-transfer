package ru.ortex.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class AccountCreateReq {

    public BigDecimal balance;
    public String name;

    @JsonCreator
    public AccountCreateReq(
            @JsonProperty(value = "balance", required = true) BigDecimal balance,
            @JsonProperty(value = "name", required = true) String name
    ) {
        this.balance = balance;
        this.name = name;
    }

    @Override
    public String toString() {
        return "AccountCreateReq{" +
                "name='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }
}
