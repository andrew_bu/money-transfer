package ru.ortex.api;

import ru.ortex.generated.tables.pojos.Account;
import ru.ortex.generated.tables.pojos.Transaction;

import java.util.List;

public class AccountInfo extends Account {

    public final List<Transaction> transactions;

    public AccountInfo(Account account, List<Transaction> transactions) {
        super(account);
        this.transactions = transactions;
    }
}
