package ru.ortex.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class TransactionCreateReq {

    public long fromAccId;
    public long toAccId;
    public BigDecimal amount;

    @JsonCreator
    public TransactionCreateReq(
            @JsonProperty(value = "fromAccId", required = true) long fromAccId,
            @JsonProperty(value = "toAccId", required = true) long toAccId,
            @JsonProperty(value = "amount", required = true) BigDecimal amount
    ) {
        this.fromAccId = fromAccId;
        this.toAccId = toAccId;
        this.amount = amount;
    }
}
