package ru.ortex;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class ContextManager {

    private static Logger logger = LoggerFactory.getLogger(Main.class);

    private final DataSource dataSource;

    public ContextManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public <T> T run(Action<T> action) {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);

            T result = action.run(DSL.using(conn));

            conn.commit();
            conn.close();
            return result;

        } catch (Exception e) {

            if (conn != null) {
                try {
                    conn.rollback();
                    conn.close();
                } catch (SQLException rollbackException) {
                    logger.error("rollback exception", e);
                }
            }
            if (e instanceof RuntimeException) {
                throw (RuntimeException) e;
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    @FunctionalInterface
    public interface Action<T> {
        T run(DSLContext context);
    }
}
