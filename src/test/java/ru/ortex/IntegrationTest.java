package ru.ortex;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Dsl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ortex.exception.Exceptions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Semaphore;

import static spark.Spark.awaitInitialization;

public class IntegrationTest {

    private static final String BASE_URI = "http://localhost:4567/";
    private static final ObjectMapper MAPPER = new ObjectMapper();

    private AsyncHttpClient httpClient;

    @Before
    public void setUp() {
        Main.main(new String[]{});
        awaitInitialization();

        httpClient = Dsl.asyncHttpClient();
    }

    @After
    public void tearDown() throws Exception {
        httpClient.close();
    }

    @Test
    public void simpleTransactionTest() throws Exception {

        JsonNode response1 = createAccount("Account #1", BigDecimal.valueOf(1000)).get();
        long accId1 = response1.get("id").asLong();

        JsonNode response2 = createAccount("Account #2", BigDecimal.valueOf(1000)).get();
        long accId2 = response2.get("id").asLong();

        transaction(accId1, accId2, BigDecimal.valueOf(500)).get();

        JsonNode acc1 = getAccount(accId1).get();
        Assert.assertEquals(BigDecimal.valueOf(500.0), acc1.get("balance").decimalValue());

        JsonNode acc2 = getAccount(accId2).get();
        Assert.assertEquals(BigDecimal.valueOf(1500.0), acc2.get("balance").decimalValue());
    }

    @Test
    public void thereAndBackAgainMoneyTest() throws Exception {

        BigDecimal startBalance = BigDecimal.valueOf(1000000.0);
        JsonNode response1 = createAccount("Account #1", startBalance).get();
        long accId1 = response1.get("id").asLong();

        JsonNode response2 = createAccount("Account #2", startBalance).get();
        long accId2 = response2.get("id").asLong();

        List<CompletableFuture<JsonNode>> listFutures = new ArrayList<>();

        Semaphore throttler = new Semaphore(10);
        for (int i = 0; i < 1000; i++) {
            throttler.acquire();
            CompletableFuture<JsonNode> transaction = i % 10 == 0
                    ? transaction(accId1, accId2, BigDecimal.valueOf(90))
                    : transaction(accId2, accId1, BigDecimal.valueOf(10));

            transaction.whenComplete((response, throwable) -> throttler.release());
            listFutures.add(transaction);
        }

        listFutures.forEach(future -> Exceptions.uncheck(future::get));

        JsonNode acc1 = getFullAccount(accId1).get();
        Assert.assertEquals(startBalance, acc1.get("balance").decimalValue());
        Assert.assertEquals(1000, acc1.get("transactions").size());

        JsonNode acc2 = getFullAccount(accId2).get();
        Assert.assertEquals(startBalance, acc2.get("balance").decimalValue());
        Assert.assertEquals(1000, acc2.get("transactions").size());
    }

    @Test
    public void circleTransactionTest() throws Exception {

        BigDecimal startBalance = BigDecimal.valueOf(1000000.0);
        JsonNode response1 = createAccount("Account #1", startBalance).get();
        long accId1 = response1.get("id").asLong();

        JsonNode response2 = createAccount("Account #2", startBalance).get();
        long accId2 = response2.get("id").asLong();

        JsonNode response3 = createAccount("Account #3", startBalance).get();
        long accId3 = response3.get("id").asLong();

        List<CompletableFuture<JsonNode>> listFutures = new ArrayList<>();

        Semaphore throttler = new Semaphore(10);
        for (int i = 0; i < 3 * 400; i++) {
            throttler.acquire();

            CompletableFuture<JsonNode> transaction;
            switch (i % 3) {
                case 0:
                    transaction = transaction(accId1, accId2, BigDecimal.valueOf(50));
                    break;
                case 1:
                    transaction = transaction(accId2, accId3, BigDecimal.valueOf(50));
                    break;
                case 2:
                    transaction = transaction(accId3, accId1, BigDecimal.valueOf(50));
                    break;
                default:
                    throw new RuntimeException();
            }

            transaction.whenComplete((response, throwable) -> throttler.release());
            listFutures.add(transaction);
        }

        listFutures.forEach(future -> Exceptions.uncheck(future::get));

        JsonNode acc1 = getAccount(accId1).get();
        Assert.assertEquals(startBalance, acc1.get("balance").decimalValue());

        JsonNode acc2 = getAccount(accId2).get();
        Assert.assertEquals(startBalance, acc2.get("balance").decimalValue());

        JsonNode acc3 = getAccount(accId3).get();
        Assert.assertEquals(startBalance, acc3.get("balance").decimalValue());
    }

    private CompletableFuture<JsonNode> createAccount(String name, BigDecimal balance) {

        String url = BASE_URI + "accounts";

        String body = MAPPER.createObjectNode()
                .put("name", name)
                .put("balance", balance)
                .toString();

        return post(url, body);
    }

    private CompletableFuture<JsonNode> transaction(long accId1, long accId2, BigDecimal amount) {

        String url = BASE_URI + "transactions";

        String body = MAPPER.createObjectNode()
                .put("fromAccId", accId1)
                .put("toAccId", accId2)
                .put("amount", amount)
                .toString();

        return post(url, body);
    }

    private CompletableFuture<JsonNode> getAccount(long accId) {
        String url = BASE_URI + "account/" + accId;
        return get(url);
    }

    private CompletableFuture<JsonNode> getFullAccount(long accId) {
        String url = BASE_URI + "account/full/" + accId;
        return get(url);
    }

    private CompletableFuture<JsonNode> get(String url) {
        return httpClient
                .prepareGet(url)
                .execute()
                .toCompletableFuture()
                .thenApply(resp -> {
                    Assert.assertEquals(200, resp.getStatusCode());
                    return parseJson(resp.getResponseBody());
                });
    }

    private CompletableFuture<JsonNode> post(String url, String body) {
        return httpClient
                .preparePost(url)
                .setBody(body)
                .execute()
                .toCompletableFuture()
                .thenApply(resp -> {
                    Assert.assertEquals(200, resp.getStatusCode());
                    return parseJson(resp.getResponseBody());
                });
    }

    private static JsonNode parseJson(String responseBody) {
        return Exceptions.uncheck(() -> MAPPER.readTree(responseBody));
    }

}
