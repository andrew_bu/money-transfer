package ru.ortex.service;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.ortex.generated.tables.pojos.Account;
import ru.ortex.generated.tables.pojos.Transaction;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;

public class TransactionalServiceTest {

    @Rule
    public TestContextManager testContextManager = new TestContextManager();

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();


    @Test
    public void notEnoughMoneyTest() {
        AccountService accountService = new AccountService(testContextManager.get());
        TransactionalService transactionalService = new TransactionalService(testContextManager.get());

        long accountId1 = accountService.create("Account1", BigDecimal.ONE);
        long accountId2 = accountService.create("Account2", BigDecimal.ONE);

        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage(allOf(containsString("not enough money"), containsString("to transfer 10")));
        transactionalService.transfer(accountId1, accountId2, BigDecimal.TEN);
    }

    @Test
    public void accountNotFoundTest() {
        AccountService accountService = new AccountService(testContextManager.get());
        TransactionalService transactionalService = new TransactionalService(testContextManager.get());

        long accountId1 = accountService.create("Account1", BigDecimal.TEN);

        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("Account[8764] not found");
        transactionalService.transfer(accountId1, 8764, BigDecimal.ONE);
    }

    @Test
    public void transactionToSameAccountTest() {
        AccountService accountService = new AccountService(testContextManager.get());
        TransactionalService transactionalService = new TransactionalService(testContextManager.get());

        long accountId1 = accountService.create("Account1", BigDecimal.TEN);

        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("Source and target accounts should be different!");
        transactionalService.transfer(accountId1, accountId1, BigDecimal.ONE);
    }

    @Test
    public void transferMoneyTest() {
        AccountService accountService = new AccountService(testContextManager.get());
        TransactionalService transactionalService = new TransactionalService(testContextManager.get());

        long accountId1 = accountService.create("Account1", BigDecimal.TEN);
        long accountId2 = accountService.create("Account2", BigDecimal.TEN);

        long transactionId = transactionalService.transfer(accountId1, accountId2, BigDecimal.TEN);

        Account account1 = accountService.getById(accountId1);
        Account account2 = accountService.getById(accountId2);

        Assert.assertEquals(BigDecimal.ZERO, account1.getBalance());
        Assert.assertEquals(BigDecimal.valueOf(20), account2.getBalance());

        Transaction transaction = transactionalService.getById(transactionId);

        Assert.assertEquals(BigDecimal.TEN, transaction.getAmount());
        Assert.assertEquals(accountId1, (long) transaction.getFromAccountId());
        Assert.assertEquals(accountId2, (long) transaction.getToAccountId());
    }
}