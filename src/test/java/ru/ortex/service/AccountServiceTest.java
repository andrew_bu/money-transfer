package ru.ortex.service;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import ru.ortex.generated.tables.pojos.Account;

import java.math.BigDecimal;

public class AccountServiceTest {

    @Rule
    public TestContextManager testContextManager = new TestContextManager();

    @Test
    public void test() {

        AccountService service = new AccountService(testContextManager.get());
        long accId = service.create("acc1", BigDecimal.TEN);

        Account account = service.getById(accId);

        Assert.assertEquals("acc1", account.getName());
        Assert.assertEquals(BigDecimal.TEN, account.getBalance());
    }

}
