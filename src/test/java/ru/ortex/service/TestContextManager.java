package ru.ortex.service;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.junit.rules.ExternalResource;
import ru.ortex.ContextManager;

import javax.sql.DataSource;

public class TestContextManager extends ExternalResource {

    private HikariDataSource dataSource;

    @Override
    protected void before() {
        HikariConfig config = new HikariConfig(this.getClass().getClassLoader().getResource("application.properties").getFile());
        dataSource = new HikariDataSource(config);
    }

    @Override
    protected void after() {
        dataSource.close();
    }

    public ContextManager get() {
        return new ContextManager(dataSource);
    }

    public DataSource getDataSource() {
        return dataSource;
    }

}
