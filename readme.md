## Money transfer application

Provide api for money transfers between accounts.

### Build

Build fat jar and run:
```
./gradlew build
java -jar ./build/libs/money-transfer-fat-jat-1.0-SNAPSHOT.jar

```


### API

##### POST /accounts
Create account.   
Request body: `{"name": <name>, "balance": <balance>}`  
Response body: `{"id": <id>}`

##### GET /accounts/:id
Retrieve account by id.   
Response body: `{"id":<id>,"name":<name>,"balance":<balance>,"createdDate":<created_date>,"updatedDate":<updated_date>}`

##### GET /accounts/full/:id
Retrieve account by id and all transactions.  
Response body: `{"id":<id>,"name":<name>,"balance":<balance>,"createdDate":<created_date>,"updatedDate":<updated_date>,"transaction":[<transaction>,...]}`

##### POST /transactions
Create transaction from one account to another.  
Request body: `{"fromAccId":<from_account_id>,"toAccId":<to_account_id>,"amount":<amount>}`  
Response body: `{"id": <id>}`

##### GET /transaction/:id
Retrieve transaction by id.  
Response body: `{"id":<id>,"name":<name>,"balance":<balance>,"createdDate":<created_date>,"updatedDate":<updated_date>,"complete":<complete>}`